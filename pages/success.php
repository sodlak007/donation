	
	<div class="row">
    <!-- Col MD -->
    <div class="col-md-12">	
      <div class="row">
        <div class="other-pages">
          <div class="col-md-2 col-sm-1 col-xs-12"></div>
          <div class="col-md-8 col-sm-10 col-xs-12">
            <section>
              <center>
                <p class="page-title">Success Stories</p>
              </center>

              <div class="success-story clearfix">
                <div class="row">
                  <div class="success-image">
                    <img src="https://dthsik0xywgi1.cloudfront.net/16841288_1482077452.2858.jpg" style="border-radius: 3px;">
                    <p style="margin-bottom: 0; margin-top: 5px;"><strong>$87,587</strong> raised of $100,000</p>
                  </div>
                  <div class="story-div">
                    <div class="story">
                      <h4>24/7 Home Care for World's Oldest WWII Veteran</h4>
                      <p>In just one week, over $85,000 was raised to help the oldest living WWII veteran afford in-home care and stay in his beloved home.</p>
                    </div>
                  </div>
                </div>
              </div>

              <div class="success-story clearfix">
                <div class="row">
                  <div class="success-image">
                    <img src="https://dthsik0xywgi1.cloudfront.net/16854932_1482119522.119.jpg" style="border-radius: 3px;">
                    <p style="margin-bottom: 0; margin-top: 5px;"><strong>$87,587</strong> raised of $100,000</p>
                  </div>
                  <div class="story-div">
                    <div class="story">
                      <h4>Save Circus Lions From Colombia</h4>
                      <p>Animal lovers around the world rallied together to rescue these lions that had been trapped in cages for an entire year. After their goal was met in just two weeks, these cats will be getting a new, loving home.</p>
                    </div>
                  </div>
                </div>
              </div>
            </section>

          </div>
          <div class="col-md-2 col-sm-1 col-xs-12"></div>
        </div>
      </div>
    </div><!-- /COL MD -->
  </div>
 </div><!-- row -->