<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Payment;

/**
 * Description of StripePaymentManager
 *
 * @author olaniyi
 */
class StripePaymentManager extends PaymentManager{
    //put your code here
    
    public function processPayment(Campaigns $campaign) {
        $email    = $this->request->email;
		$cents    = bcmul($this->request->amount, 100);
		$amount = (int)$cents;
		$currency_code = $this->settings->currency_code;
		$description = trans('misc.donation_for').' '.$campaign->title;
		$nameSite = $this->settings->title;
 
		
		if( isset( $this->request->stripeToken ) ) {
			
				\Stripe\Stripe::setApiKey($this->settings->stripe_secret_key);
				
						
				// Get the credit card details submitted by the form
				$token = $this->request->stripeToken;
				
				// Create a charge: this will charge the user's card
				try {
				  $charge = \Stripe\Charge::create(array(
				    "amount" => $amount, // Amount in cents
				    "currency" => strtolower($currency_code),
				    "source" => $token,
				    "description" => $description
				    ));
					
					if( !isset( $this->request->anonymous ) ) {
						$this->request->anonymous = '0';
					}
					
					// Insert DB and send Mail
				  $sql = new Donations;
		          $sql->campaigns_id     = $campaign->id;
				  $sql->txn_id                  = 'null';
				  $sql->fullname              = $this->request->full_name;
				  $sql->email                   = $this->request->email;
				  $sql->country                = $this->request->country;
				  $sql->postal_code          = $this->request->postal_code;
				  $sql->donation                = $this->request->amount;
				  $sql->payment_gateway  = 'Stripe';
				  $sql->comment               = $this->request->comment;
				  $sql->anonymous           = $this->request->anonymous;
				  $sql->save();
				  
				  $sender           = $this->settings->email_no_reply;
				  $titleSite          = $this->settings->title;
				  $_emailUser    = $this->request->email;
				  $campaignID   = $campaign->id;
				  $fullNameUser = $this->request->fullname;
				  
				  Mail::send('emails.thanks-donor', array( 'data' => $campaignID, 'fullname' => $fullNameUser, 'title_site' => $titleSite ), 
					function($message) use ( $sender, $fullNameUser, $titleSite, $_emailUser)
						{
						    $message->from($sender, $titleSite)
						    	->to($_emailUser, $fullNameUser)
								->subject( trans('misc.thanks_donation').' - '.$titleSite );
						});
			
			return response()->json([
				        'success' => true,
				        'stripeSuccess' => true,
				        'url' => url('paypal/donation/success',$campaign->id)
				    ]);
													
				} catch(\Stripe\Error\Card $e) {
				  // The card has been declined
				}
		} else {
			return response()->json([
				        'success' => true,
				        'stripeTrue' => true,
				        "key" => $this->settings->stripe_public_key,
				        "email" => $email,
				         "amount" => $amount,
					    "currency" => strtoupper($currency_code),
					    "description" => $description,
					    "name" => $nameSite
				    ]);
		}
    }
}
