<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Helper;

use App\Helper\Auth;
use App\Helper\MoneywaveClient;
use App\Helper\TransactionManager;
use App\Models\Campaigns;
use App\Helper\Config;

/**
 * Description of MoneywaveManager
 *
 * @author olaniyi
 */
class MoneywaveManager extends PaymentManager {

    private $auth;
    public $isRedirect = false;
    public $redirectHTML = "";
    private $moneywaveClient;
    private $configData;

    public function __construct() {
        $this->auth = Auth::getInstance();
        $this->moneywaveClient = new MoneywaveClient($this->auth);
        $this->configData = Config::getInstance()->getData();
    }

    /**
     * charges card and moves payment to wallet
     * @param \Payment\Campaigns $campaign 
     * 
     */
    public function processPayment(Campaigns $campaign, $requestData, $paymentInfo = []) {

        $response = array();

        $paymentResponse = $this->moneywaveClient->transfer($paymentInfo, MoneywaveClient::$RECIPIENT_WALLET);

        if ($paymentResponse["status"] == "success") {
            $response["success"] = true;
            $response["paymentUrl"] = $this->configData["app"]["base_url"] . "paymentResponse";

            $this->storeDonationPaymentRequest($campaign, $paymentResponse["data"], $requestData);

            session(["paymentResponseHTML" => $paymentResponse["data"]["responsehtml"]]);
        } else {

            $response["success"] = false;
        }

        $response["paymentResponse"] = $paymentResponse;

        return $response;
    }

    public function setRedirectData($paymentResponse) {
        if (!$paymentResponse["transfer"]["isCardValidationSuccessful"]) {
            $this->isRedirect = true;
            $this->redirectHTML = $paymentResponse["responsehtml"];
        }
    }

    public function storeDonationPaymentRequest($campaign, $paymentResponseData, $requestData) {
        $donation = new \App\Models\Donations();


        $donation->campaigns_id = $campaign->id;
        $donation->txn_id = $paymentResponseData["transfer"]["flutterChargeReference"];
        $donation->firstname = $paymentResponseData["transfer"]["firstName"];
        $donation->lastname = $paymentResponseData["transfer"]["lastName"];
        $donation->email = $requestData["email"];
        $donation->donation = $paymentResponseData["transfer"]["amountToSend"];
        $donation->payment_gateway = "flutterwave";
        $donation->comment = $requestData["comment"];
        $donation->date = date("Y-m-d h:i:s");
        $donation->anonymous = empty($requestData["anonymous"]) ? 0 : 1;
        $donation->is_paid = 0;

        $donation->save();
    }

    /**
     * @param type $requestData
     */
    public function getPaymentInfo($requestData) {

        $cardExpirySplit = explode("/", $requestData["cardExpiry"]);

        $expiryMonth = intval(trim($cardExpirySplit[0]));
        $expiryYear = intval(trim($cardExpirySplit[1]));

        $paymentInfo = array(
            TransactionManager::AMOUNT => $requestData["amount"],
            TransactionManager::PHONE_NUMBER => $requestData["phone_number"],
            TransactionManager::EMAIL => $requestData["email"],
            TransactionManager::CARD_NO => $requestData["cardNumber"],
            TransactionManager::CVV => $requestData["cardCVC"],
            TransactionManager::EXPIRY_YEAR => $expiryYear,
            TransactionManager::EXPIRY_MONTH => $expiryMonth,
            TransactionManager::FEE => 0,
            TransactionManager::REDIRECT_URL => $this->configData["app"]["base_url"] . "finalise-payment",
            TransactionManager::MEDIUM => "web"
        );
        $names = explode(" ", $requestData["full_name"]);
        $firstname = $names[0];
        $lastname = $names[1];
        $paymentInfo[TransactionManager::FIRSTNAME] = $firstname;
        $paymentInfo[TransactionManager::LASTNAME] = $lastname;

        return $paymentInfo;
    }

    public function listBanks() {

        $data = $this->moneywaveClient->listBanks();

        if ($data["status"] == "success") {
            return $this->moneywaveClient->listBanks()["data"];
        }

        return [];
    }

}
