<?php

namespace App\Helper;

use App\Helper\Config;
use GuzzleHttp\Client;

class Auth {

    protected static $instance;
    protected $client;
    protected $baseUrl;
    protected $apiKey;
    protected $apiSecret;

    public function __construct() {
        $this->client = new Client();
        $data = Config::getInstance()->getData();
        $this->baseUrl = $data["api"]["base_url"];
        $this->apiKey = $data["auth"]["api_key"];
        $this->apiSecret = $data["auth"]["api_secret"];
    }

    /**
     * 
     * @return this
     */
    public static function getInstance() {

        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function getAccessToken() {

        $url = $this->baseUrl . "v1/merchant/verify";


        $body = ["apiKey" => $this->apiKey, "secret" => $this->apiSecret];

        $options = ["headers" => Config::$jsonHeader, "body" => json_encode($body)];


        $request = $this->client->request(POST, $url, $options);
        // $request->
        $response = json_decode($request->getBody()->getContents(), true);

        if ($response["status"] == "success") {
            return $response["token"];
        }

        return false;
    }

}
