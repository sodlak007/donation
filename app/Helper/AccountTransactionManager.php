<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Moneywave;
/**
 * Description of AccountTransactionManager
 *
 * @author olaniyi
 */
class AccountTransactionManager extends AbstractTransactionManager {

    //put your code here
    public function __set($name, $value) {
        $this->$name = $value;
    }

    public function __get($name) {
        return $this->$name;
    }

    public function toArray() {
        $data = array(self::FIRSTNAME => $this->firstname,
            self::LASTNAME => $this->lastname,
            self::PHONE_NUMBER => $this->phonenumber,
            self::AMOUNT => $this->amount,
            self::API_KEY => $this->apiKey,
            self::CARD_LAST4 => $this->cardLast4,
            self::CARD_NO => $this->cardNo,
            self::CHARGE_CURRENCY => $this->chargeCurrency,
            self::CHARGE_WITH => $this->chargeWith,
            self::CVV => $this->cvv,
            self::DISBURSE_CURRENCY => $this->disburseCurrency,
            self::EMAIL => $this->email,
            self::EXPIRY_MONTH => $this->expiryMonth,
            self::EXPIRY_YEAR => $this->expiryYear,
            self::FEE => $this->fee,
            self::MEDIUM => $this->medium,
            self::RECIPIENT_ACCOUNT_NUMBER => $this->recipientAccountNumber,
            self::RECIPIENT_BANK => $this->recipientBank,
            self::REDIRECT_URL => $this->redirectUrl,
            self::SENDER_ACCOUNT_NUMBER => $this->senderAccountNumber,
            self::SENDER_BANK => $this->senderBank
        );
        
        return $data;
    }

}
