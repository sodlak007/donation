<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use App\Models;
use App\Models\Campaigns;

namespace App\Helper;

/**
 * Description of PaymentManager
 *
 * @author olaniyi
 */
abstract class PaymentManager {

    //put your code here
    public abstract function processPayment(\App\Models\Campaigns $campaign, $requestData, $paymentInfo = []);
}
