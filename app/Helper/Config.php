<?php

namespace App\Helper;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require 'app_config.php';

/**
 * Description of Config
 * Contains the App's Config Settings. This settings is gotten 
 * from config.ini
 * @author olaniyi
 */
class Config {

    //put your code here
    /**
     *
     * @var this
     */
    protected static $instance;
    private $data = [];
    private $configFile = "config.ini";
    public static $jsonHeader = array(
        'content-type' => 'application/json'
    );

    /**
     *  
     */
    protected function __construct() {
        $this->loadConfigFile();
    }

    private function loadConfigFile() {
        $this->data = parse_ini_file($this->configFile, true);
    }

    /**
     * 
     * @return this
     */
    public static function getInstance() {

        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * gets config.ini data
     * @return array
     */
    public function getData() {
        return $this->data;
    }

}
