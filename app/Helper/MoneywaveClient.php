<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Helper;

use App\Helper\TransactionManager;
use Guzzle\Http\Exception\ClientErrorResponseException;

/**
 * Description of MoneywaveClient
 *
 * @author olaniyi
 */
class MoneywaveClient {

    //put your code here
    private $auth;
    private $baseUrl, $client, $apiKey, $apiSecret;
    private $lock;
    public static $RECIPIENT_WALLET = "wallet";
    public static $RECIPIENT_ACCOUNT = "account";

    public function __construct(Auth $auth) {
        $this->auth = $auth;
        $this->client = new \GuzzleHttp\Client();
        $data = Config::getInstance()->getData();
        $this->baseUrl = $data["api"]["base_url"];
        $this->apiKey = $data["auth"]["api_key"];
        $this->apiSecret = $data["auth"]["api_secret"];
        $this->lock = $data["auth"]["lock"];
    }

    /**
     * this transfer money from card to an account number
     * or to a wallet
     */
    public function transfer($data, $recipient) {

        if ($recipient = self::$RECIPIENT_WALLET) {
            $data[TransactionManager::RECIPIENT] = "wallet";
        }

        $url = $this->baseUrl . "v1/transfer";

        $options = $this->initRequest();

        $options["body"] = json_encode($data);
        try {

            $request = $this->client->request(POST, $url, $options);
            return json_decode($request->getBody()->getContents(), true);
        } catch (\GuzzleHttp\Exception\RequestException $e) {

            if ($e->hasResponse()) {
                return json_decode($e->getResponse()->getBody()->getContents(), true);
            }

            $error = ["status" => "error",
                "message" => "There was an error in completing payment, please try again",
                "code" => "INVALID_RESPONSE"];

            return $error;
        }
    }

    public function disburse($data) {
        $url = $this->baseUrl . "v1/disburse";

        $options = $this->initRequest();

        $options["body"] = $data;
        try {
            $request = $this->client->request(POST, $url, $options);

            return json_decode($request->getBody()->getContents(), true);
        } catch (\GuzzleHttp\Exception\RequestException $e) {

            if ($e->hasResponse()) {
                return json_decode($e->getResponse()->getBody()->getContents(), true);
            }

            $error = ["status" => "error", "message" => "There was an error in completing payment, please try again"];

            return $error;
        }
    }

    public function disburseBulk($recipients = [], $reference) {


        $options = $this->initRequest();

        $data = ["lock" => $this->lock, "currency" => "NGN", "senderName" => "goodgivi", "ref" => $reference];

        $data["recipients"] = $recipients;

        $options["body"] = $data;
        try {
            $request = $this->client->request(POST, $url, $options);

            return json_decode($request->getBody()->getContents(), true);
        } catch (\GuzzleHttp\Exception\RequestException $e) {

            if ($e->hasResponse()) {
                return json_decode($e->getResponse()->getBody()->getContents(), true);
            }

            $error = ["status" => "error", "message" => "There was an error in completing payment, please try again"];

            return $error;
        }
    }

  
    /**
     * 
     * @return array
     */
    public function listBanks() {
        $url = $this->baseUrl . "banks/";

        $options = $this->initRequest();

        //var_dump($options);
        try {
            $request = $this->client->request(POST, $url, $options);

            // $request->
            return json_decode($request->getBody()->getContents(), true);
        } catch (\GuzzleHttp\Exception\RequestException $e) {

            if ($e->hasResponse()) {
                return json_decode($e->getResponse()->getBody()->getContents(), true);
            }

            $error = ["status" => "error", "message" => "There was an error in completing payment, please try again"];

            return $error;
        }
    }

    /**
     * 
     * @return array
     */
    public function initRequest() {

        // $body = ["apiKey" => $this->apiKey, "secret" => $this->apiSecret];
        $headers = ['content-type' => 'application/json', "Authorization" => $this->auth->getAccessToken(), "exceptions" => FALSE];
        $options = ["headers" => $headers];

        return $options;
    }

}
