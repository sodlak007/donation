<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Payment;

/**
 * Description of PaypalManager
 *
 * @author olaniyi
 */
class PaypalManager extends PaymentManager {

    //put your code here
    private $settings;

    public function __construct() {
        $this->settings = \App\Models\AdminSettings::first();
    }

    public function processPayment(Campaigns $campaign, $paymentInfo = []) {
        $urlSuccess = url('paypal/donation/success', $campaign->id);
        $urlCancel = url('paypal/donation/cancel', $campaign->id);
        $urlPaypalIPN = url('paypal/ipn');

        return response()->json([
                    'success' => true,
                    'formPP' => '<form id="form_pp" name="_xclick" action="' . $action . '" method="post"  style="display:none">
				        <input type="hidden" name="cmd" value="_donations">
				        <input type="hidden" name="return" value="' . $urlSuccess . '">
				        <input type="hidden" name="cancel_return"   value="' . $urlCancel . '">
				        <input type="hidden" name="notify_url" value="' . $urlPaypalIPN . '">
				        <input type="hidden" name="currency_code" value="' . $this->settings->currency_code . '">
				        <input type="hidden" name="amount" id="amount" value="' . $this->request->amount . '">
				        <input type="hidden" name="custom" value="id=' . $campaign->id . '&fn=' . $this->request->full_name . '&mail=' . $this->request->email . '&cc=' . $this->request->country . '&pc=' . $this->request->postal_code . '&cm=' . $this->request->comment . '&anonymous=' . $this->request->anonymous . '">
				        <input type="hidden" name="item_name" value="' . trans('misc.donation_for') . ' ' . $campaign->title . '">
				        <input type="hidden" name="business" value="' . $this->settings->paypal_account . '">
				        <input type="submit">
				        </form>',
        ]);
    }

}
