<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Helper;
/**
 * Description of CardData
 *
 * @author olaniyi
 */
class TransactionManager {

    //put your code here
    protected $firstname;
    protected $lastname;
    protected $phonenumber;
    protected $email;
    protected $recipientBank;
    protected $recipient;
    protected $recipientAccountNumber;
    protected $cardNo;
    protected $cvv;
    protected $expiryYear;
    protected $expiryMonth;
    protected $apiKey;
    protected $amount;
    protected $fee;
    protected $redirectUrl;
    protected $medium;
    protected $chargeCurrency;
    protected $disburseCurrency;
    protected $chargeWith;
    protected $cardLast4;
    protected $senderAccountNumber;
    protected $senderBank;

    const FIRSTNAME = "firstname";
    const LASTNAME = "lastname";
    const PHONE_NUMBER = "phonenumber";
    const EMAIL = "email";
    const RECIPIENT_BANK = "recipient_bank";
    const RECIPIENT = "recipient";
    const RECIPIENT_ACCOUNT_NUMBER = "recipient_account_number";
    const CARD_NO = "card_no";
    const CVV = "cvv";
    const EXPIRY_YEAR = "expiry_year";
    const EXPIRY_MONTH = "expiry_month";
    const API_KEY = "apiKey";
    const AMOUNT = "amount";
    const FEE = "fee";
    const REDIRECT_URL = "redirecturl";
    const MEDIUM = "medium";
    const CHARGE_CURRENCY = "chargeCurrency";
    const DISBURSE_CURRENCY = "disburseCurrency";
    const CHARGE_WITH = "charge_with";
    const CARD_LAST4 = "card_last4";
    const SENDER_ACCOUNT_NUMBER = "sender_account_number";
    const SENDER_BANK = "sender_bank";
    
}
