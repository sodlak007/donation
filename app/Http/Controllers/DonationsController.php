<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\AdminSettings;
use App\Models\Campaigns;
use App\Models\Donations;
use App\Models\User;
use Fahim\PaypalIPN\PaypalIPNListener;
use App\Helper\MoneywaveManager;
use Mail;

class DonationsController extends Controller {

    public function __construct(AdminSettings $settings, Request $request) {
        $this->settings = $settings::first();
        $this->request = $request;
    }

    /**
     *  
     * @return \Illuminate\Http\Response
     */
    public function show($id, $slug = null) {

        $response = Campaigns::where('id', $id)->where('status', 'active')->firstOrFail();


        // Redirect if campaign is ended
        if ($response->finalized == 1) {
            return redirect('campaign/' . $response->id);
        }

        $uriCampaign = $this->request->path();

        if (str_slug($response->title) == '') {
            $slugUrl = '';
        } else {
            $slugUrl = '/' . str_slug($response->title);
        }

        $url_campaign = 'donate/' . $response->id . $slugUrl;

        //<<<-- * Redirect the user real page * -->>>
        $uriCanonical = $url_campaign;

        if ($uriCampaign != $uriCanonical) {
            return redirect($uriCanonical);
        }

        $data = [];


        return view('default.donate')->withResponse($response);
    }

// End Method

    public function send() {

        $messages = array(
            'amount.min' => trans('misc.amount_minimum', ['symbol' => $this->settings->currency_symbol, 'code' => $this->settings->currency_code]),
            'amount.max' => trans('misc.amount_maximum', ['symbol' => $this->settings->currency_symbol, 'code' => $this->settings->currency_code]),
        );

        $response = array();

        $moneywaveMgr = new MoneywaveManager();
        $campaign = Campaigns::findOrFail($this->request->_id);

        $validator = Validator::make($this->request->all(), [
                    'amount' => 'required|integer|min:' . $this->settings->min_donation_amount . '|max:' . $this->settings->max_donation_amount,
                    'full_name' => 'required|max:95',
                    'email' => 'required|max:100',
                    'phone_number' => 'required|max:20',
                    'comment' => 'max:100',
                        ], $messages);

        if ($validator->fails()) {
            return response()->json([
                        'success' => false,
                        'errors' => $validator->getMessageBag()->toArray(),
            ]);
        }


        if ($this->settings->payment_gateway == "Flutterwave") {
            //file_put_contents("baba.json", json_encode($this->request->all()));

            $paymentInfo = $moneywaveMgr->getPaymentInfo($this->request->all());

            //file_put_contents("sdas.json", json_encode($paymentInfo));

            $response = $moneywaveMgr->processPayment($campaign, $this->request->all(), $paymentInfo);
            return response()->json($response);
        }

        return response()->json(["success" => false, "errors" => ["No Payment Gateway Configured"]]
        );
    }

    public function finalisePayment() {
        $requestData = $this->request->all();

        $transactionStatus = $requestData["transactionStatus"];
        $reference = $requestData["ref"];
        $responseMessage = $requestData["responseMessage"];
        if ($transactionStatus == "success") {
            $donation = Donations::where("txn_id", $reference)->first();

            $donation->is_paid = 1;

            $donation->save();

            return redirect("/donate/payment-successful")->with("message", $responseMessage);
        }
        return redirect("/donate/payment-failed")->with("message", $responseMessage);
    }

// End Method

    public function paypalIpn() {

        $ipn = new PaypalIPNListener();

        $ipn->use_curl = false;

        if ($this->settings->paypal_sandbox == 'true') {
            // SandBox
            $ipn->use_sandbox = true;
        } else {
            // Real environment
            $ipn->use_sandbox = false;
        }

        $verified = $ipn->processIpn();

        //$report = Helper::checkTextDb($ipn->getTextReport()); // Report the transation

        $custom = $_POST['custom'];
        parse_str($custom, $donation);

        $payment_status = $_POST['payment_status'];
        $txn_id = $_POST['txn_id'];
        $amount = $_POST['mc_gross'];


        if ($verified) {
            if ($payment_status == 'Completed') {
                // Check outh POST variable and insert in DB

                $verifiedTxnId = Donations::where('txn_id', $txn_id)->first();

                if (!isset($verifiedTxnId)) {

                    $sql = new Donations;
                    $sql->campaigns_id = $donation['id'];
                    $sql->txn_id = $txn_id;
                    $sql->fullname = $donation['fn'];
                    $sql->email = $donation['mail'];
                    $sql->country = $donation['cc'];
                    $sql->postal_code = $donation['pc'];
                    $sql->donation = $amount;
                    $sql->payment_gateway = 'Paypal';
                    $sql->comment = $donation['cm'];
                    $sql->anonymous = $donation['anonymous'];
                    $sql->save();

                    $sender = $this->settings->email_no_reply;
                    $titleSite = $this->settings->title;
                    $_emailUser = $donation['mail'];
                    $campaignID = $donation['id'];
                    $fullNameUser = $donation['fn'];

                    Mail::send('emails.thanks-donor', array('data' => $campaignID, 'fullname' => $fullNameUser, 'title_site' => $titleSite), function($message) use ( $sender, $fullNameUser, $titleSite, $_emailUser) {
                        $message->from($sender, $titleSite)
                                ->to($_emailUser, $fullNameUser)
                                ->subject(trans('misc.thanks_donation') . ' - ' . $titleSite);
                    });
                }// <--- Verified Txn ID
            } // <-- Payment status
        } else {
            //Some thing went wrong in the payment !
        }
    }

// End Method
}
