@extends('app')

@section('title'){{ trans('misc.categories').' - ' }}@endsection

@section('content') 
<div class="jumbotron md index-header jumbotron_set jumbotron-cover">

</div>

<div class="container" style="padding:9px;">
    <div class="col-md-12">
        <div class="success-ctn">
        	<img src="{{ asset('public/img/error.png') }}" height="64">
        	<h3>Your transaction was unsuccessful</h3>
        	<p>{{ $message }}</p>
        	<a href="{{ url('/') }}" class="main-btn">Return to homepage</a>
        </div>
    </div>
</div>

@endsection



