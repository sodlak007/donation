@extends('app')

@section('title'){{ trans('misc.categories').' - ' }}@endsection

@section('content') 
<div class="jumbotron md index-header jumbotron_set jumbotron-cover">
      
</div>

<div class="container">
    <div class="col-md-12">
        <div class="success-ctn">
        	<img src="{{ asset('public/img/checked.png') }}" height="64">
        	<h3>Your transaction was successful</h3>
        	<p>Thank you for your donation. </p>
        	<a href="{{ url('/') }}" class="main-btn">Return to homepage</a>
        </div>
    </div>
</div>
            

@endsection
