<!-- ***** Footer ***** -->
<footer class="footer-main">
    <div class="container">

        <div class="row">
            <div class="col-md-4">
                <a href="{{ url('/') }}">
                    <img src="{{ asset('public/img/logo.png') }}" />
                </a>
                <p class="margin-tp-xs" style="color: #c0c0c0;font-weight: 300;">{{ $settings->description }}</p>



            </div><!-- ./End col-md-* -->



            <div class="col-md-2 margin-tp-xs">
                <h4 class="margin-top-zero"><!-- {{trans('misc.about')}} --> Get Started</h4>
                <ul class="list-unstyled">
                    <!-- @foreach( App\Models\Pages::all() as $page )
            <li><a class="link-footer" href="{{ url('/page',$page->slug) }}">{{ $page->title }}</a></li>
@endforeach -->
                    <li><a class="link-footer" href="{{ url('/hiw') }}">How it Works</a></li>
                    {{-- <li><a class="link-footer" href="{{ url('/') }}">Pricing and Fees</a></li> --}}
                    <li><a class="link-footer" href="{{ url('/faq') }}">Common Questions</a></li>
                    <li><a class="link-footer" href="{{ url('/success') }}">Success Stories</a></li>
                    <li><a class="link-footer" href="{{ url('/create/campaign') }}">Create a Campaign</a></li>
                </ul>
            </div><!-- ./End col-md-* -->


            <div class="col-md-2 margin-tp-xs">
                <h4 class="margin-top-zero">About Us</h4>
                <ul class="list-unstyled">
                    <!-- @foreach(  App\Models\Categories::where('mode','on')->orderBy('name')->take(6)->get() as $category )
            <li><a class="link-footer" href="{{ url('category') }}/{{ $category->slug }}">{{ $category->name }}</a></li>
@endforeach

@if( App\Models\Categories::count() > 6 )
    <li><a class="link-footer" href="{{ url('categories') }}">
            <strong>{{ trans('misc.view_all') }} <i class="fa fa-long-arrow-right"></i></strong>
                     </a></li>
                     @endif -->

                    <li><a class="link-footer" href="{{ url('/page',$page->slug) }}">About Us</a></li>
                    {{--<li><a class="link-footer" href="{{ url('/page',$page->slug) }}">Blog</a></li> --}}
                    {{-- <li><a class="link-footer" href="{{ url('/page',$page->slug) }}">Press</a></li> --}}
                    <li><a class="link-footer" href="{{ url('/terms') }}">Terms & Conditions</a></li>

                </ul>
            </div><!-- ./End col-md-* -->

            <div class="col-md-2 margin-tp-xs">
               {{-- <h4 class="margin-top-zero">Get Support</h4>
                <ul class="list-unstyled">

                    {{--<li><a class="link-footer" href="{{ url('/') }}">Ask a Question</a></li>--}}
                    {{--<li><a class="link-footer" href="{{ url('/page',$page->slug) }}">Help Center</a></li>--}}
                    <{{--li><a class="link-footer" href="{{ url('/page',$page->slug) }}">Supported Countries & Currencies</a></li>

                    <!-- @if( Auth::guest() )
                    <li>
                            <a class="link-footer" href="{{ url('login') }}">
                                    {{ trans('auth.login') }}
                            </a>
                            </li>


                            @else
                            <li>
                                    <a href="{{ url('account') }}" class="link-footer">
                                            {{ trans('users.account_settings') }}
                                    </a>
                                    </li>

                                    <li>
                                            <a href="{{ url('logout') }}" class="logout link-footer">
                                                    {{ trans('users.logout') }}
                                            </a>
                                    </li>
                            @endif -->

                </ul> --}}
            </div><!-- ./End col-md-* -->


            <div class="col-md-2 margin-tp-xs">
                <h4 class="margin-top-zero">Social Media</h4>
                <ul class="list-inline">
                    @if( $settings->twitter != '' ) 
                    <li><a href="{{$settings->twitter}}" class="ico-social"><i class="fa fa-twitter"></i></a></li>
                    @endif

                    @if( $settings->facebook != '' )   
                    <li><a href="{{$settings->facebook}}" class="ico-social"><i class="fa fa-facebook"></i></a></li>
                    @endif

                    @if( $settings->instagram != '' )   
                    <li><a href="{{$settings->instagram}}" class="ico-social"><i class="fa fa-instagram"></i></a></li>
                    @endif

                    @if( $settings->linkedin != '' )   
                    <li><a href="{{$settings->linkedin}}" class="ico-social"><i class="fa fa-linkedin"></i></a></li>
                    @endif

                    @if( $settings->googleplus != '' )   
                    <li><a href="{{$settings->googleplus}}" class="ico-social"><i class="fa fa-google-plus"></i></a></li>
                    @endif
                </ul >
            </div><!-- ./End col-md-* -->


        </div><!-- ./End Row -->
    </div><!-- ./End Container -->
</footer><!-- ***** Footer ***** -->

<footer class="subfooter">
    <div class="container">
        <div class="row">
            <div class="col-md-12  padding-top-20">
                <p style=" color: #c0c0c0; font-size: 12px; font-weight: 300; border-top: 1px solid #595959; padding-top: 20px">&copy; {{ $settings->title }} - <?php echo date('Y'); ?></p>
            </div><!-- ./End col-md-* -->
        </div>
    </div>
</footer>    
