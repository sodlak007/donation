<div class="posts margin-bottom-30" id="posts" style="padding-top:20px;">
	<div class="row" id="campaigns">
		<div class="col-md-12 btn-block margin-bottom-20 clearfix">
			<h3 class="" style="margin-top: 0;">Featured Campaigns</h3>
			<h5 class=""><!-- {{trans('misc.recent_campaigns')}} --> <a href="#">View all</a></h5>
		</div>	
   @foreach ( $data as $key )
    	@include('includes.list-campaigns')
    	  @endforeach
    	 <div class="col-xs-12 loadMoreSpin">
    	 		{{ $data->links('vendor.pagination.loadmore') }}
    	 </div>
    	 </div><!-- /row -->
 </div><!-- ./ End Posts -->


 <div class="posts margin-bottom-30" id="posts" style="padding-top:20px;">
	<div class="row" id="campaigns">
		<div class="col-md-12 btn-block margin-bottom-20 clearfix">
			<h3 class="">Popular Campaigns</h3>
			<h5 class=""><!-- {{trans('misc.recent_campaigns')}} --> <a href="#">View all</a></h5>
		</div>	
   @foreach ( $data as $key )
    	@include('includes.list-campaigns')
    	  @endforeach
    	 <div class="col-xs-12 loadMoreSpin">
    	 		{{ $data->links('vendor.pagination.loadmore') }}
    	 </div>
    	 </div><!-- /row -->
 </div><!-- ./ End Posts -->


  <div class="posts margin-bottom-30" id="posts" style="padding-top:20px;">
	<div class="row" id="campaigns">
		<div class="col-md-12 btn-block margin-bottom-20 clearfix">
			<h3 class="">Latest Campaigns</h3>
			<h5 class=""><!-- {{trans('misc.recent_campaigns')}} --> <a href="#">View all</a></h5>
		</div>	
   @foreach ( $data as $key )
    	@include('includes.list-campaigns')
    	  @endforeach
    	 <div class="col-xs-12 loadMoreSpin">
    	 		{{ $data->links('vendor.pagination.loadmore') }}
    	 </div>
    	 </div><!-- /row -->
 </div><!-- ./ End Posts -->


  <div class="posts margin-bottom-30" id="posts" style="padding-top:20px;">
	<div class="row" id="campaigns">
		<div class="col-md-12 btn-block margin-bottom-20 clearfix">
			<h3 class="">Ending Soon</h3>
			<h5 class=""><!-- {{trans('misc.recent_campaigns')}} --> <a href="#">View all</a></h5>
		</div>	
   @foreach ( $data as $key )
    	@include('includes.list-campaigns')
    	  @endforeach
    	 <div class="col-xs-12 loadMoreSpin">
    	 		{{ $data->links('vendor.pagination.loadmore') }}
    	 </div>
    	 </div><!-- /row -->
 </div><!-- ./ End Posts -->