@extends('app')

@section('title') {{ $title }} @endsection

@section('content') 
<div class="jumbotron md index-header jumbotron_set jumbotron-cover">
      <div class="container wrap-jumbotron position-relative">
        <h2 class="title-site">{{$title}}</h2>
      </div>
    </div>

<div class="container margin-bottom-40">
	
	<div class="row">
    <!-- Col MD -->
    <div class="col-md-12">	
      <div class="row">
        <div class="other-pages">
          <div class="col-md-1 col-sm-12 col-xs-12"></div>
          <div class="col-md-10 col-sm-12 col-xs-12">
            <section>
               <center>
                <p class="page-title">Getting Started</p>
              </center>
              <div class="quest-main">
                <div class="question-ctn">
                  <p class="quest">How does it work? <i class="glyphicon glyphicon-triangle-bottom myicon-right"></i></p>
                  <div class="answer">
                    <p>Goodgivi.ng makes it incredibly easy to raise money online for the things that matter to you most. In less than a minute, you'll be able to personalize your fundraising campaign and share it with the people in your life. In fact, over $3 Billion has been raised by Goodgivi.ng users just like you.</p>
                    <p>With Goodgivi.ng, the money you collect goes directly to you. There are no deadlines or limits - each donation you receive is yours to keep. Raising money for yourself or a loved one has never been easier. Plus, we're here to help. Goodgivi.ng will respond to your emails within 5 minutes during the day.</p>
                  </div>
                </div>

                <div class="question-ctn">
                  <p class="quest">What can i raise money for? <i class="glyphicon glyphicon-triangle-bottom myicon-right"></i></p>
                  <div class="answer">
                    <p>Goodgivi.ng makes it incredibly easy to raise money online for the things that matter to you most. In less than a minute, you'll be able to personalize your fundraising campaign and share it with the people in your life. In fact, over $3 Billion has been raised by Goodgivi.ng users just like you.</p>
                    <p>With Goodgivi.ng, the money you collect goes directly to you. There are no deadlines or limits - each donation you receive is yours to keep. Raising money for yourself or a loved one has never been easier. Plus, we're here to help. Goodgivi.ng will respond to your emails within 5 minutes during the day.</p>
                  </div>
                </div>
              </div>
            </section>
          </div>
          <div class="col-md-1 col-sm-13 col-xs-12"></div>
        </div>
      </div>
    </div><!-- /COL MD -->
  </div>
 
 </div><!-- row -->
 
 <!-- container wrap-ui -->
@endsection

