@extends('app')

@section('title') {{ $title }} @endsection

@section('content') 
<div class="jumbotron md index-header jumbotron_set jumbotron-cover">
    <div class="container wrap-jumbotron position-relative">
        <h2 class="title-site">{{$title}}</h2>
    </div>
</div>

<div class="container margin-bottom-40">
    <div class="row">
        <!-- Col MD -->
        <div class="col-md-12">	
            <div class="row">
                <div class="other-pages">
                    <div class="col-md-1 col-sm-12 col-xs-12"></div>
                    <div class="col-md-10 col-sm-12 col-xs-12">
                        <section>
                            <center>
                                <p class="page-title">What you get</p>
                            </center>

                            <center>
                                <div class="listss">
                                    <ul>
                                        <li><i class="glyphicon glyphicon-ok myicon-right"></i> Mobile-Friendly Campaigns</li>
                                        <li><i class="glyphicon glyphicon-ok myicon-right"></i> No deadlines or goal requirements</li>
                                        <li><i class="glyphicon glyphicon-ok myicon-right"></i> No penalties for missing goal</li>
                                        <li><i class="glyphicon glyphicon-ok myicon-right"></i> 5-Minute Email Support</li>
                                        <li><i class="glyphicon glyphicon-ok myicon-right"></i> GoFundMe Mobile App</li>
                                        <li><i class="glyphicon glyphicon-ok myicon-right"></i> Keep every donation you receive</li>
                                    </ul>
                                </div>
                            </center>

                            <center>
                                <div class="cta-bottom">
                                    <a href="#">Ask Questions</a>
                                </div>
                            </center>
                        </section>

                        <section>
                            <center>
                                <p class="page-title" style="margin-bottom: 40px;">Pricing</p>
                            </center>

                            <div class="row margin-bottom-40" >
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="pricing-box">
                                        <div class="pricing-percentage">5%</div>
                                        <div class="pricing-detail">GoFundMe Platform</div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="pricing-box">
                                        <div class="pricing-percentage">2.9%</div>
                                        <div class="pricing-detail">+ $0.30 Payment Processing</div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="pricing-box">
                                        <div class="pricing-percentage">7.9%</div>
                                        <div class="pricing-detail">+ $0.30 per donation</div>
                                    </div>
                                </div>
                            </div>

                            <center>
                                <p style="font-weight: 600;margin-bottom: 0px;">Charity Campaigns</p>
                                <p style="font-size: 12px;color: #666;margin-bottom: 30px;">Raise money that goes directly to an NPO.</p>
                            </center>

                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="pricing-box">
                                        <div class="pricing-percentage">5%</div>
                                        <div class="pricing-detail">GoFundMe Platform</div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="pricing-box">
                                        <div class="pricing-percentage">2.9%</div>
                                        <div class="pricing-detail">+ $0.30 Payment Processing</div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="pricing-box">
                                        <div class="pricing-percentage">7.9%</div>
                                        <div class="pricing-detail">+ $0.30 per donation</div>
                                    </div>
                                </div>
                            </div>

                            <div class="footer-box">
                                <div class="row">
                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                        <p class="cq">Common Questions</p>
                                        <p>Learn more about GoFundMe and how it can help you fundraise.</p>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="cta-bottom">
                                            <a href="#" class="opp">Ask Questions</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                    </div>
                    <div class="col-md-1 col-sm-13 col-xs-12"></div>
                </div>
            </div>
        </div><!-- /COL MD -->
    </div></div>
@endsection

